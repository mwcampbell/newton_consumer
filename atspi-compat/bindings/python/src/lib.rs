// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

use ::newton_atspi_compat as lib;
use pyo3::{exceptions::*, prelude::*};
use std::{
    collections::{HashMap, VecDeque},
    hash::{DefaultHasher, Hash, Hasher},
    sync::Mutex,
};

fn map_error(error: lib::Error) -> PyErr {
    PyRuntimeError::new_err(format!("{}", error))
}

fn coord_type_from_u32(value: u32) -> PyResult<lib::CoordType> {
    match value {
        0 => Ok(lib::CoordType::Screen),
        1 => Ok(lib::CoordType::Window),
        2 => Ok(lib::CoordType::Parent),
        _ => Err(PyValueError::new_err("invalid CoordType")),
    }
}

fn granularity_from_u32(value: u32) -> PyResult<lib::Granularity> {
    match value {
        0 => Ok(lib::Granularity::Char),
        1 => Ok(lib::Granularity::Word),
        2 => Ok(lib::Granularity::Sentence),
        3 => Ok(lib::Granularity::Line),
        4 => Ok(lib::Granularity::Paragraph),
        _ => Err(PyValueError::new_err("invalid Granularity")),
    }
}

#[pyclass(frozen, module = "newton_atspi_compat")]
struct Range {
    start_offset: i32,
    end_offset: i32,
}

#[pymethods]
impl Range {
    fn __eq__(&self, other: &Self) -> bool {
        self.start_offset == other.start_offset && self.end_offset == other.end_offset
    }

    #[getter]
    fn start_offset(&self) -> i32 {
        self.start_offset
    }

    #[getter]
    fn end_offset(&self) -> i32 {
        self.end_offset
    }
}

#[pyclass(module = "newton_atspi_compat")]
struct Rect(lib::Rect);

#[pymethods]
impl Rect {
    fn __eq__(&self, other: &Self) -> bool {
        self.0 == other.0
    }

    #[getter]
    fn x(&self) -> i32 {
        self.0.x
    }

    #[setter]
    fn set_x(&mut self, value: i32) {
        self.0.x = value;
    }

    #[getter]
    fn y(&self) -> i32 {
        self.0.y
    }

    #[setter]
    fn set_y(&mut self, value: i32) {
        self.0.y = value;
    }

    #[getter]
    fn width(&self) -> i32 {
        self.0.width
    }

    #[setter]
    fn set_width(&mut self, value: i32) {
        self.0.width = value;
    }

    #[getter]
    fn height(&self) -> i32 {
        self.0.height
    }

    #[setter]
    fn set_height(&mut self, value: i32) {
        self.0.height = value;
    }
}

#[pyclass(frozen, module = "newton_atspi_compat")]
struct TextRange {
    content: String,
    start_offset: i32,
    end_offset: i32,
}

#[pymethods]
impl TextRange {
    fn __eq__(&self, other: &Self) -> bool {
        self.content == other.content
            && self.start_offset == other.start_offset
            && self.end_offset == other.end_offset
    }

    #[getter]
    fn content(&self) -> &str {
        &self.content
    }

    #[getter]
    fn start_offset(&self) -> i32 {
        self.start_offset
    }

    #[getter]
    fn end_offset(&self) -> i32 {
        self.end_offset
    }
}

#[pyclass(frozen, module = "newton_atspi_compat")]
struct Accessible(lib::simplified::Accessible);

#[pymethods]
impl Accessible {
    fn __eq__(&self, other: &Self) -> bool {
        self.0 == other.0
    }

    fn __hash__(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.0.hash(&mut hasher);
        hasher.finish()
    }

    fn get_role(&self) -> PyResult<u32> {
        self.0.role().map(|role| role as u32).map_err(map_error)
    }

    fn get_localized_role_name(&self) -> PyResult<String> {
        self.0.localized_role_name().map_err(map_error)
    }

    fn get_name(&self) -> PyResult<String> {
        self.0.name().map_err(map_error)
    }

    fn get_description(&self) -> PyResult<String> {
        self.0.description().map_err(map_error)
    }

    fn get_locale(&self) -> &str {
        // TODO: once it's implemented in AccessKit
        ""
    }

    fn get_state_bits(&self) -> u64 {
        self.0.state().bits()
    }

    fn state_set_as_string(&self) -> String {
        let mut result = String::new();
        for state in self.0.state().iter() {
            if !result.is_empty() {
                result.push_str(", ");
            }
            let state_string = String::from(state);
            result.push_str(&state_string);
        }
        result
    }

    fn get_parent(&self) -> PyResult<Option<Self>> {
        self.0
            .parent()
            .map(|parent| parent.map(Self))
            .map_err(map_error)
    }

    fn get_index_in_parent(&self) -> PyResult<i32> {
        self.0.index_in_parent().map_err(map_error)
    }

    fn get_child_count(&self) -> PyResult<i32> {
        self.0.child_count().map_err(map_error)
    }

    fn get_child_at_index(&self, index: usize) -> PyResult<Option<Self>> {
        self.0
            .child_at_index(index)
            .map(|child| child.map(Self))
            .map_err(map_error)
    }

    fn get_children(&self) -> PyResult<Vec<Self>> {
        self.0.map_children(Self).map_err(map_error)
    }

    fn get_application(&self) -> PyResult<Self> {
        self.0.application().map(Self).map_err(map_error)
    }

    fn get_application_toolkit_name(&self) -> PyResult<String> {
        self.0.toolkit_name().map_err(map_error)
    }

    fn get_application_toolkit_version(&self) -> PyResult<String> {
        self.0.toolkit_version().map_err(map_error)
    }

    fn supports_action(&self) -> PyResult<bool> {
        self.0.supports_action().map_err(map_error)
    }

    fn get_n_actions(&self) -> PyResult<i32> {
        self.0.n_actions().map_err(map_error)
    }

    fn get_action_name(&self, index: i32) -> PyResult<String> {
        self.0.action_name(index).map_err(map_error)
    }

    fn do_action(&self, index: i32) -> PyResult<bool> {
        self.0.do_action(index).map_err(map_error)
    }

    fn supports_component(&self) -> PyResult<bool> {
        self.0.supports_component().map_err(map_error)
    }

    fn contains(&self, x: i32, y: i32, coord_type: u32) -> PyResult<bool> {
        self.0
            .contains(x, y, coord_type_from_u32(coord_type)?)
            .map_err(map_error)
    }

    fn get_accessible_at_point(&self, x: i32, y: i32, coord_type: u32) -> PyResult<Option<Self>> {
        self.0
            .accessible_at_point(x, y, coord_type_from_u32(coord_type)?)
            .map(|accessible| accessible.map(Self))
            .map_err(map_error)
    }

    fn get_extents(&self, coord_type: u32) -> PyResult<Rect> {
        self.0
            .extents(coord_type_from_u32(coord_type)?)
            .map(Rect)
            .map_err(map_error)
    }

    fn grab_focus(&self) -> PyResult<bool> {
        self.0.grab_focus().map_err(map_error)
    }

    fn scroll_to_point(&self, coord_type: u32, x: i32, y: i32) -> PyResult<bool> {
        self.0
            .scroll_to_point(coord_type_from_u32(coord_type)?, x, y)
            .map_err(map_error)
    }

    fn supports_text(&self) -> PyResult<bool> {
        self.0.supports_text().map_err(map_error)
    }

    fn get_character_count(&self) -> PyResult<i32> {
        self.0.character_count().map_err(map_error)
    }

    fn get_caret_offset(&self) -> PyResult<i32> {
        self.0.caret_offset().map_err(map_error)
    }

    fn get_string_at_offset(&self, offset: i32, granularity: u32) -> PyResult<TextRange> {
        self.0
            .string_at_offset(offset, granularity_from_u32(granularity)?)
            .map(|(content, start_offset, end_offset)| TextRange {
                content,
                start_offset,
                end_offset,
            })
            .map_err(map_error)
    }

    fn get_text(&self, start_offset: i32, end_offset: i32) -> PyResult<String> {
        self.0.text(start_offset, end_offset).map_err(map_error)
    }

    fn set_caret_offset(&self, offset: i32) -> PyResult<bool> {
        self.0.set_caret_offset(offset).map_err(map_error)
    }

    fn get_character_extents(&self, offset: i32, coord_type: u32) -> PyResult<Rect> {
        self.0
            .character_extents(offset, coord_type_from_u32(coord_type)?)
            .map(Rect)
            .map_err(map_error)
    }

    fn get_offset_at_point(&self, x: i32, y: i32, coord_type: u32) -> PyResult<i32> {
        self.0
            .offset_at_point(x, y, coord_type_from_u32(coord_type)?)
            .map_err(map_error)
    }

    fn get_n_selections(&self) -> PyResult<i32> {
        self.0.n_selections().map_err(map_error)
    }

    fn get_selection(&self, selection_num: i32) -> PyResult<Range> {
        self.0
            .selection(selection_num)
            .map(|(start_offset, end_offset)| Range {
                start_offset,
                end_offset,
            })
            .map_err(map_error)
    }

    fn add_selection(&self, start_offset: i32, end_offset: i32) -> PyResult<bool> {
        self.0
            .add_selection(start_offset, end_offset)
            .map_err(map_error)
    }

    fn remove_selection(&self, selection_num: i32) -> PyResult<bool> {
        self.0.remove_selection(selection_num).map_err(map_error)
    }

    fn set_selection(
        &self,
        selection_num: i32,
        start_offset: i32,
        end_offset: i32,
    ) -> PyResult<bool> {
        self.0
            .set_selection(selection_num, start_offset, end_offset)
            .map_err(map_error)
    }

    fn get_range_extents(
        &self,
        start_offset: i32,
        end_offset: i32,
        coord_type: u32,
    ) -> PyResult<Rect> {
        self.0
            .range_extents(start_offset, end_offset, coord_type_from_u32(coord_type)?)
            .map(Rect)
            .map_err(map_error)
    }

    fn get_attribute_run(
        &self,
        offset: i32,
        include_defaults: bool,
    ) -> PyResult<(HashMap<String, String>, i32, i32)> {
        self.0
            .text_attribute_run(offset, include_defaults)
            .map_err(map_error)
    }

    fn supports_value(&self) -> PyResult<bool> {
        self.0.supports_value().map_err(map_error)
    }

    fn get_minimum_value(&self) -> PyResult<f64> {
        self.0.minimum_value().map_err(map_error)
    }

    fn get_maximum_value(&self) -> PyResult<f64> {
        self.0.maximum_value().map_err(map_error)
    }

    fn get_minimum_increment(&self) -> PyResult<f64> {
        self.0.minimum_increment().map_err(map_error)
    }

    fn get_current_value(&self) -> PyResult<f64> {
        self.0.current_value().map_err(map_error)
    }

    fn set_current_value(&self, value: f64) -> PyResult<()> {
        self.0.set_current_value(value).map_err(map_error)
    }
}

#[pyclass(frozen, module = "newton_atspi_compat")]
struct Event(lib::simplified::Event);

#[pymethods]
impl Event {
    fn __eq__(&self, other: &Self) -> bool {
        self.0 == other.0
    }

    #[getter]
    fn r#type(&self) -> &str {
        &self.0.kind
    }

    #[getter]
    fn source(&self) -> Accessible {
        Accessible(self.0.source.clone())
    }

    #[getter]
    fn detail1(&self) -> i32 {
        self.0.detail1
    }

    #[getter]
    fn detail2(&self) -> i32 {
        self.0.detail2
    }

    #[getter]
    fn any_data(&self, py: Python) -> Py<PyAny> {
        match &self.0.data {
            None => py.None(),
            Some(lib::simplified::EventData::U32(value)) => (*value).into_py(py),
            Some(lib::simplified::EventData::F64(value)) => (*value).into_py(py),
            Some(lib::simplified::EventData::String(value)) => value.into_py(py),
            Some(lib::simplified::EventData::Rect(value)) => Rect(*value).into_py(py),
            Some(lib::simplified::EventData::Accessible(accessible)) => {
                Accessible(accessible.clone()).into_py(py)
            }
        }
    }
}

struct EventHandler {
    queue: Mutex<VecDeque<lib::simplified::Event>>,
    callback: Py<PyAny>,
    key_event_callback: Py<PyAny>,
}

impl EventHandler {
    fn new(callback: Py<PyAny>, key_event_callback: Py<PyAny>) -> Self {
        Self {
            queue: Mutex::new(VecDeque::new()),
            callback,
            key_event_callback,
        }
    }
}

impl lib::EventHandler for EventHandler {
    fn enqueue_event(&self, adapter: &lib::Adapter, event: lib::Event) {
        let event = lib::simplified::Event::new(adapter, event);
        self.queue.lock().unwrap().push_back(event);
    }

    fn handle_pointer_moved_event(&self, adapter: &lib::Adapter, surface_x: f64, surface_y: f64) {
        let root = adapter.platform_node(adapter.root_id());
        let event = lib::simplified::Event {
            kind: "newton:pointer-moved".into(),
            source: lib::simplified::Accessible::Node(root),
            detail1: surface_x as i32,
            detail2: surface_y as i32,
            data: None,
        };
        Python::with_gil(|py| {
            self.callback.call(py, (Event(event),), None).unwrap();
        });
    }

    fn handle_key_event(
        &self,
        released: bool,
        state: u32,
        keysym: u32,
        unichar: u32,
        keycode: u16,
    ) {
        Python::with_gil(|py| {
            self.key_event_callback
                .call(py, (released, state, keysym, unichar, keycode), None)
                .unwrap();
        });
    }

    fn handle_events(&self) {
        Python::with_gil(|py| {
            let mut queue = self.queue.lock().unwrap();
            while let Some(event) = queue.pop_front() {
                self.callback.call(py, (Event(event),), None).unwrap();
            }
        });
    }
}

#[pyclass(frozen, module = "newton_atspi_compat")]
pub struct Consumer(lib::Consumer);

#[pymethods]
impl Consumer {
    #[new]
    fn new(event_handler: Py<PyAny>, key_event_handler: Py<PyAny>) -> Self {
        Self(lib::Consumer::new(Box::new(EventHandler::new(
            event_handler,
            key_event_handler,
        ))))
    }

    fn grab_keyboard(&self) {
        self.0.grab_keyboard();
    }

    fn ungrab_keyboard(&self) {
        self.0.ungrab_keyboard();
    }

    fn set_key_grabs(&self, modifiers: Vec<u32>, keystrokes: Vec<(u32, u32)>) {
        self.0.set_key_grabs(&modifiers, &keystrokes);
    }
}

#[pymodule]
fn newton_atspi_compat(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add_class::<Range>()?;
    m.add_class::<Rect>()?;
    m.add_class::<TextRange>()?;
    m.add_class::<Accessible>()?;
    m.add_class::<Event>()?;
    m.add_class::<Consumer>()?;

    Ok(())
}

// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

// Derived from smithay-clipboard.
// Copyright (c) 2018 Lucas Timmins & Victor Berger
// Licensed under the MIT license (found in the LICENSE-MIT file).

use tokio::sync::mpsc::{unbounded_channel, UnboundedSender};
use zbus::Connection;

mod proxy;
mod shared;
mod state;
mod worker;

pub use shared::*;

use proxy::ManagerProxy;

pub struct Consumer {
    request_tx: UnboundedSender<worker::Command>,
    worker_thread: Option<std::thread::JoinHandle<()>>,
}

impl Consumer {
    pub fn new(event_handler: Box<dyn EventHandler + Send>) -> Self {
        let runtime = tokio::runtime::Builder::new_current_thread()
            .enable_io()
            .enable_time()
            .build()
            .unwrap();
        let (manager, initial_focus_surface_id) = runtime.block_on(async {
            let connection = Connection::session().await.unwrap();
            let manager = ManagerProxy::new(&connection).await.unwrap();
            let initial_focus_surface_id = manager.get_keyboard_focus().await.unwrap();
            (manager, initial_focus_surface_id)
        });
        let (request_tx, request_rx) = unbounded_channel();
        let worker_thread = worker::spawn(
            runtime,
            manager,
            initial_focus_surface_id,
            request_tx.clone(),
            request_rx,
            event_handler,
        );

        Self {
            request_tx,
            worker_thread,
        }
    }

    pub fn grab_keyboard(&self) {
        self.request_tx.send(worker::Command::GrabKeyboard).unwrap();
    }

    pub fn ungrab_keyboard(&self) {
        self.request_tx
            .send(worker::Command::UngrabKeyboard)
            .unwrap();
    }

    pub fn set_key_grabs(&self, modifiers: &[u32], keystrokes: &[(u32, u32)]) {
        self.request_tx
            .send(worker::Command::SetKeyGrabs {
                modifiers: Box::from(modifiers),
                keystrokes: Box::from(keystrokes),
            })
            .unwrap();
    }
}

impl Drop for Consumer {
    fn drop(&mut self) {
        let _ = self.request_tx.send(worker::Command::Exit);
        if let Some(worker_thread) = self.worker_thread.take() {
            let _ = worker_thread.join();
        }
    }
}

// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

// Derived from smithay-clipboard.
// Copyright (c) 2018 Lucas Timmins & Victor Berger
// Licensed under the MIT license (found in the LICENSE-MIT file).

use accesskit::{ActionRequest, TreeUpdate};
use std::{
    collections::HashMap,
    os::fd::AsFd,
    sync::atomic::{AtomicUsize, Ordering},
};
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::unix::pipe::{pipe, Receiver},
    runtime::Handle as RuntimeHandle,
    sync::{mpsc::UnboundedSender, Mutex},
};
use zbus::{
    zvariant::{Fd, ObjectPath},
    Connection, SignalContext,
};

use crate::{
    proxy::ManagerProxy,
    shared::{EventHandler, SurfaceHandle},
    worker::Command,
};

pub(crate) fn send_action_request(
    surface_id: u32,
    request: ActionRequest,
    runtime_handle: &RuntimeHandle,
    manager: &ManagerProxy<'static>,
) {
    let serialized = serde_json::to_vec(&request).unwrap();
    let (mut sender, receiver) = pipe().unwrap();
    let receiver = receiver.into_blocking_fd().unwrap();
    runtime_handle.spawn(async move {
        let _ = sender.write_all(&serialized).await;
    });
    let manager = manager.clone();
    runtime_handle.spawn(async move {
        manager
            .do_action(surface_id, Fd::Owned(receiver))
            .await
            .unwrap();
    });
}

static NEXT_UPDATE_SINK_ID: AtomicUsize = AtomicUsize::new(0);

struct UpdateSink {
    surface_id: u32,
    request_tx: UnboundedSender<Command>,
    // The following is to prevent updates from being processed concurrently
    // and possibly finished out of order.
    mutex: Mutex<()>,
}

#[zbus::interface(name = "org.freedesktop.a11y.UpdateSink")]
impl UpdateSink {
    async fn update(&self, fd: Fd<'_>) {
        let guard = self.mutex.lock().await;
        let fd = fd.as_fd().try_clone_to_owned().unwrap();
        let mut receiver = Receiver::from_owned_fd(fd).unwrap();
        let mut buffer = Vec::new();
        if receiver.read_to_end(&mut buffer).await.is_err() {
            return;
        }
        self.request_tx
            .send(Command::HandleUpdate {
                surface_id: self.surface_id,
                buffer,
            })
            .unwrap();
        drop(guard);
    }

    #[zbus(signal)]
    async fn closed(ctxt: &SignalContext<'_>) -> zbus::Result<()>;
}

pub(crate) struct SurfaceState {
    update_sink_path: Option<ObjectPath<'static>>,
}

impl SurfaceState {
    fn new() -> Self {
        Self {
            update_sink_path: None,
        }
    }

    pub(crate) fn start_updates(
        &mut self,
        id: u32,
        runtime_handle: &RuntimeHandle,
        manager: &ManagerProxy<'static>,
        request_tx: &UnboundedSender<Command>,
    ) {
        if self.update_sink_path.is_some() {
            return;
        }
        let sink_id = NEXT_UPDATE_SINK_ID.fetch_add(1, Ordering::Relaxed);
        let path = ObjectPath::from_string_unchecked(format!("/UpdateSink/{}", sink_id));
        self.update_sink_path = Some(path.clone());
        let sink = UpdateSink {
            surface_id: id,
            request_tx: request_tx.clone(),
            mutex: Mutex::new(()),
        };
        let manager = manager.clone();
        runtime_handle.spawn(async move {
            manager
                .inner()
                .connection()
                .object_server()
                .at(&path, sink)
                .await
                .unwrap();
            manager.request_surface_updates(id, &path).await.unwrap();
        });
    }

    pub(crate) fn stop_updates(&mut self, runtime_handle: &RuntimeHandle, connection: &Connection) {
        let path = match self.update_sink_path.take() {
            Some(path) => path,
            None => return,
        };
        let connection = connection.clone();
        runtime_handle.spawn(async move {
            let object_server = connection.object_server();
            let iface_ref = object_server
                .interface::<_, UpdateSink>(&path)
                .await
                .unwrap();
            UpdateSink::closed(iface_ref.signal_context())
                .await
                .unwrap();
            object_server.remove::<UpdateSink, _>(path).await.unwrap();
        });
    }
}

pub(crate) struct State {
    runtime_handle: RuntimeHandle,
    manager: ManagerProxy<'static>,
    focused_surface_id: Option<u32>,
    surfaces: HashMap<u32, SurfaceState>,
    event_handler: Box<dyn EventHandler>,
    request_tx: UnboundedSender<Command>,
}

impl State {
    pub(crate) fn new(
        runtime_handle: &RuntimeHandle,
        manager: ManagerProxy<'static>,
        initial_focus_surface_id: u32,
        event_handler: Box<dyn EventHandler>,
        request_tx: UnboundedSender<Command>,
    ) -> Self {
        let mut state = Self {
            runtime_handle: runtime_handle.clone(),
            manager,
            focused_surface_id: None,
            surfaces: HashMap::new(),
            event_handler,
            request_tx,
        };

        state.keyboard_focus_changed(initial_focus_surface_id);

        state
    }

    fn init_surface_state_if_needed(&mut self, id: u32) {
        self.surfaces.entry(id).or_insert_with(SurfaceState::new);
    }

    pub(crate) fn keyboard_focus_changed(&mut self, surface_id: u32) {
        if let Some(old_surface_id) = self.focused_surface_id.take() {
            let old_surface_state = self.surfaces.get_mut(&old_surface_id).unwrap();
            let mut handle = SurfaceHandle {
                id: old_surface_id,
                has_focus: false,
                state: old_surface_state,
                runtime_handle: &self.runtime_handle,
                manager: &self.manager,
                request_tx: &self.request_tx,
            };
            self.event_handler.focus_lost(&mut handle);
        }
        self.focused_surface_id = Some(surface_id);
        self.init_surface_state_if_needed(surface_id);
        let surface_state = self.surfaces.get_mut(&surface_id).unwrap();
        let mut handle = SurfaceHandle {
            id: surface_id,
            has_focus: true,
            state: surface_state,
            runtime_handle: &self.runtime_handle,
            manager: &self.manager,
            request_tx: &self.request_tx,
        };
        self.event_handler.focus_gained(&mut handle);
    }

    pub(crate) fn pointer_moved(&mut self, surface_id: u32, surface_x: f64, surface_y: f64) {
        self.init_surface_state_if_needed(surface_id);
        let surface_state = self.surfaces.get_mut(&surface_id).unwrap();
        let mut handle = SurfaceHandle {
            id: surface_id,
            has_focus: self.focused_surface_id == Some(surface_id),
            state: surface_state,
            runtime_handle: &self.runtime_handle,
            manager: &self.manager,
            request_tx: &self.request_tx,
        };
        self.event_handler
            .pointer_moved(&mut handle, surface_x, surface_y);
    }

    pub(crate) fn surface_destroyed(&mut self, id: u32) {
        let mut surface_state = match self.surfaces.remove(&id) {
            Some(state) => state,
            None => return,
        };
        surface_state.stop_updates(&self.runtime_handle, self.manager.inner().connection());
        if self.focused_surface_id == Some(id) {
            self.focused_surface_id = None;
        }
        self.event_handler.surface_removed(id);
    }

    pub(crate) fn key_event(
        &mut self,
        released: bool,
        state: u32,
        keysym: u32,
        unichar: u32,
        keycode: u16,
    ) {
        self.event_handler
            .key_event(released, state, keysym, unichar, keycode);
    }

    pub(crate) fn start_updates(&mut self, surface_id: u32) {
        let surface_state = match self.surfaces.get_mut(&surface_id) {
            Some(state) => state,
            None => return,
        };
        surface_state.start_updates(
            surface_id,
            &self.runtime_handle,
            &self.manager,
            &self.request_tx,
        );
    }

    pub(crate) fn stop_updates(&mut self, surface_id: u32) {
        let surface_state = match self.surfaces.get_mut(&surface_id) {
            Some(state) => state,
            None => return,
        };
        surface_state.stop_updates(&self.runtime_handle, self.manager.inner().connection());
    }

    pub(crate) fn send_action_request(&mut self, surface_id: u32, request: ActionRequest) {
        send_action_request(surface_id, request, &self.runtime_handle, &self.manager);
    }

    pub(crate) fn handle_update(&mut self, surface_id: u32, buffer: Vec<u8>) {
        let surface_state = match self.surfaces.get_mut(&surface_id) {
            Some(state) => state,
            None => return,
        };
        let update = match serde_json::from_slice::<TreeUpdate>(&buffer) {
            Ok(update) => update,
            Err(_) => return,
        };
        let mut handle = SurfaceHandle {
            id: surface_id,
            has_focus: self.focused_surface_id == Some(surface_id),
            state: surface_state,
            runtime_handle: &self.runtime_handle,
            manager: &self.manager,
            request_tx: &self.request_tx,
        };
        self.event_handler.update(&mut handle, update);
    }

    pub(crate) fn grab_keyboard(&self) {
        let manager = self.manager.clone();
        self.runtime_handle.spawn(async move {
            manager.grab_keyboard().await.unwrap();
        });
    }

    pub(crate) fn ungrab_keyboard(&self) {
        let manager = self.manager.clone();
        self.runtime_handle.spawn(async move {
            manager.ungrab_keyboard().await.unwrap();
        });
    }

    pub(crate) fn set_key_grabs(&self, modifiers: Box<[u32]>, keystrokes: Box<[(u32, u32)]>) {
        let manager = self.manager.clone();
        self.runtime_handle.spawn(async move {
            manager
                .set_key_grabs(&modifiers, &keystrokes)
                .await
                .unwrap();
        });
    }
}

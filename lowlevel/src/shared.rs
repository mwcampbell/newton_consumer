// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

use accesskit::{ActionRequest, TreeUpdate};
use tokio::{runtime::Handle as RuntimeHandle, sync::mpsc::UnboundedSender};

use crate::{
    proxy::ManagerProxy,
    state::{send_action_request, SurfaceState},
    worker::Command,
};

pub struct SurfaceHandle<'a> {
    pub(crate) id: u32,
    pub(crate) has_focus: bool,
    pub(crate) state: &'a mut SurfaceState,
    pub(crate) runtime_handle: &'a RuntimeHandle,
    pub(crate) manager: &'a ManagerProxy<'static>,
    pub(crate) request_tx: &'a UnboundedSender<Command>,
}

impl SurfaceHandle<'_> {
    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn has_focus(&self) -> bool {
        self.has_focus
    }

    pub fn start_updates(&mut self) {
        self.state
            .start_updates(self.id, self.runtime_handle, self.manager, self.request_tx);
    }

    pub fn stop_updates(&mut self) {
        self.state
            .stop_updates(self.runtime_handle, self.manager.inner().connection());
    }

    pub fn send_action_request(&mut self, request: ActionRequest) {
        send_action_request(self.id, request, self.runtime_handle, self.manager);
    }

    pub fn global(&self) -> GlobalSurfaceHandle {
        GlobalSurfaceHandle {
            id: self.id,
            request_tx: self.request_tx.clone(),
        }
    }
}

pub struct GlobalSurfaceHandle {
    id: u32,
    request_tx: UnboundedSender<Command>,
}

impl GlobalSurfaceHandle {
    pub fn id(&self) -> u32 {
        self.id
    }

    pub fn start_updates(&mut self) {
        self.request_tx
            .send(Command::StartUpdates {
                surface_id: self.id,
            })
            .unwrap();
    }

    pub fn stop_updates(&mut self) {
        self.request_tx
            .send(Command::StopUpdates {
                surface_id: self.id,
            })
            .unwrap();
    }

    pub fn send_action_request(&mut self, request: ActionRequest) {
        self.request_tx
            .send(Command::SendActionRequest {
                surface_id: self.id,
                request,
            })
            .unwrap();
    }
}

pub trait EventHandler {
    fn surface_removed(&mut self, id: u32);
    fn focus_gained(&mut self, surface: &mut SurfaceHandle);
    fn focus_lost(&mut self, surface: &mut SurfaceHandle);
    fn pointer_moved(&mut self, surface: &mut SurfaceHandle, surface_x: f64, surface_y: f64);
    fn update(&mut self, surface: &mut SurfaceHandle, update: TreeUpdate);
    fn key_event(&mut self, released: bool, state: u32, keysym: u32, unichar: u32, keycode: u16);
}

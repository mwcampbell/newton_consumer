use zbus::{proxy, zvariant::Fd, Result};

#[proxy(
    interface = "org.freedesktop.a11y.Manager",
    default_service = "org.freedesktop.a11y.Manager",
    default_path = "/org/freedesktop/a11y/Manager"
)]
trait Manager {
    fn do_action(&self, surface_id: u32, fd: Fd<'_>) -> Result<()>;

    fn get_keyboard_focus(&self) -> Result<u32>;

    fn grab_keyboard(&self) -> Result<()>;

    fn request_surface_updates(
        &self,
        surface_id: u32,
        sink_path: &zbus::zvariant::ObjectPath<'_>,
    ) -> Result<()>;

    fn set_key_grabs(&self, modifiers: &[u32], keystrokes: &[(u32, u32)]) -> Result<()>;

    fn ungrab_keyboard(&self) -> Result<()>;

    #[zbus(signal)]
    fn keyboard_focus_changed(&self, surface_id: u32) -> Result<()>;

    #[zbus(signal)]
    fn pointer_moved(&self, surface_id: u32, surface_x: f64, surface_y: f64) -> Result<()>;

    #[zbus(signal)]
    fn surface_destroyed(&self, surface_id: u32) -> Result<()>;

    #[zbus(signal)]
    fn key_event(
        &self,
        released: bool,
        state: u32,
        keysym: u32,
        unichar: u32,
        keycode: u16,
    ) -> Result<()>;
}

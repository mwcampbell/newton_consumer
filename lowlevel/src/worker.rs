// Copyright 2024 GNOME Foundation. All rights reserved.
// Licensed under the Apache License, Version 2.0 (found in
// the LICENSE-APACHE file) or the MIT license (found in
// the LICENSE-MIT file), at your option.

// Derived from smithay-clipboard.
// Copyright (c) 2018 Lucas Timmins & Victor Berger
// Licensed under the MIT license (found in the LICENSE-MIT file).

use accesskit::ActionRequest;
use tokio::{
    pin,
    runtime::{Handle as RuntimeHandle, Runtime},
    select,
    sync::mpsc::{UnboundedReceiver, UnboundedSender},
};
use tokio_stream::{wrappers::UnboundedReceiverStream, StreamExt};

use crate::proxy::ManagerProxy;
use crate::shared::EventHandler;
use crate::state::State;

pub(crate) fn spawn(
    runtime: Runtime,
    manager: ManagerProxy<'static>,
    initial_focus_surface_id: u32,
    request_tx: UnboundedSender<Command>,
    request_rx: UnboundedReceiver<Command>,
    event_handler: Box<dyn EventHandler + Send>,
) -> Option<std::thread::JoinHandle<()>> {
    std::thread::Builder::new()
        .name("newton-consumer".into())
        .spawn(move || {
            runtime.block_on(worker_impl(
                runtime.handle(),
                manager,
                initial_focus_surface_id,
                request_tx,
                request_rx,
                event_handler,
            ));
        })
        .ok()
}

pub(crate) enum Command {
    StartUpdates {
        surface_id: u32,
    },
    StopUpdates {
        surface_id: u32,
    },
    SendActionRequest {
        surface_id: u32,
        request: ActionRequest,
    },
    HandleUpdate {
        surface_id: u32,
        buffer: Vec<u8>,
    },
    GrabKeyboard,
    UngrabKeyboard,
    SetKeyGrabs {
        modifiers: Box<[u32]>,
        keystrokes: Box<[(u32, u32)]>,
    },
    Exit,
}

async fn worker_impl(
    runtime_handle: &RuntimeHandle,
    manager: ManagerProxy<'static>,
    initial_focus_surface_id: u32,
    request_tx: UnboundedSender<Command>,
    request_rx: UnboundedReceiver<Command>,
    event_handler: Box<dyn EventHandler>,
) {
    let keyboard_focus_changed = manager
        .receive_keyboard_focus_changed()
        .await
        .unwrap()
        .fuse();
    pin!(keyboard_focus_changed);

    let pointer_moved = manager.receive_pointer_moved().await.unwrap().fuse();
    pin!(pointer_moved);

    let surface_destroyed = manager.receive_surface_destroyed().await.unwrap().fuse();
    pin!(surface_destroyed);

    let key_event = manager.receive_key_event().await.unwrap().fuse();
    pin!(key_event);

    let request_rx = UnboundedReceiverStream::new(request_rx);
    pin!(request_rx);

    let mut state = State::new(
        runtime_handle,
        manager,
        initial_focus_surface_id,
        event_handler,
        request_tx,
    );

    loop {
        select! {
            signal = keyboard_focus_changed.next() => {
                if let Some(signal) = signal {
                    let args = signal.args().unwrap();
                    state.keyboard_focus_changed(args.surface_id);
                }
            }
            signal = pointer_moved.next() => {
                if let Some(signal) = signal {
                    let args = signal.args().unwrap();
                    state.pointer_moved(args.surface_id, args.surface_x, args.surface_y);
                }
            }
            signal = surface_destroyed.next() => {
                if let Some(signal) = signal {
                    let args = signal.args().unwrap();
                    state.surface_destroyed(args.surface_id);
                }
            }
            signal = key_event.next() => {
                if let Some(signal) = signal {
                    let args = signal.args().unwrap();
                    state.key_event(args.released, args.state, args.keysym, args.unichar, args.keycode);
                }
            }
            request = request_rx.next() => {
                if let Some(request) = request {
                    match request {
                        Command::StartUpdates { surface_id } => state.start_updates(surface_id),
                        Command::StopUpdates { surface_id } => state.stop_updates(surface_id),
                        Command::SendActionRequest {
                            surface_id,
                            request,
                        } => state.send_action_request(surface_id, request),
                        Command::HandleUpdate { surface_id, buffer } => state.handle_update(surface_id, buffer),
                        Command::GrabKeyboard => state.grab_keyboard(),
                        Command::UngrabKeyboard => state.ungrab_keyboard(),
                        Command::SetKeyGrabs { modifiers, keystrokes } => state.set_key_grabs(modifiers, keystrokes),
                        Command::Exit => break,
                    }
                } else {
                    break;
                }
            }
        }
    }
}
